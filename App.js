import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import inicialScreen from './src/Telas/inicialScreen'
import CadastroScreen from './src/Telas/cadastroScreen'
import LoginScreen from './src/Telas/loginScreen';
import HomeScreen from './src/Telas/homeScrenn';
import Cabecalho from './component/cabecalho';

const Stack = createStackNavigator();

export default function App() {
  return ( 
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerTitle: () => <Cabecalho/>, }}>
        <Stack.Screen name='RGT' component={inicialScreen} options={{ title: 'RGT', headerTitleStyle: { color: 'orange' } }}/>
        <Stack.Screen name='Cadastro'component={CadastroScreen} options={{ title: 'RGT', headerTitleStyle: { color: 'orange' } }}/>
        <Stack.Screen name='Login' component={LoginScreen} options={{ title: 'RGT', headerTitleStyle: { color: 'orange' } }}/>
        <Stack.Screen name='Home' component={HomeScreen} options={{ title: 'RGT', headerTitleStyle: { color: 'orange' } }}/>
      </Stack.Navigator>
    </NavigationContainer>  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    marginTop: 20,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});




