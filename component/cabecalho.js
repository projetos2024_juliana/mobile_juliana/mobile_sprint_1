import React from 'react';
import { Image, Text, StyleSheet, TextInput } from 'react-native';
import { View } from 'react-native';

const   Cabecalho  = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center',  }}>
        <Image source={require('../assets/Logo.png')} style={{ width: 40, height: 60, marginLeft: 4 }} />
        <Text style = {styles.textInput}>RGT</Text>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    textInput: {
        color:'orange',
        marginLeft: 10,
        fontSize: 24, /* tamanho da fonte */
        fontWeight:'bold', /* texto em negrito */
    }
  })
  
export default Cabecalho;
