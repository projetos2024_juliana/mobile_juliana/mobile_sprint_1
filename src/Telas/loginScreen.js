import React from "react";
import { View, TextInput, Button, StyleSheet, Text } from "react-native";

const LoginScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
        <Text style = {styles.TextInput}>FAÇA LOGIN AQUI!!!</Text>
      <TextInput style={styles.input} placeholder="Email"/>
      <TextInput style={styles.input} placeholder="Senha" secureTextEntry={true}/>
      <View style={styles.buttonContainer}>
        <Button style={styles.button} title="Início" onPress={() => navigation.navigate('RGT')} color="orange" />
        <Button style={styles.button} title="Fazer Login" onPress={() => navigation.navigate('Home')} color="orange" />
        <Button style={styles.button} title="Cadastro" onPress={() => navigation.navigate('Cadastro')} color="orange" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: "center", 
    alignItems:"center", 
    paddingHorizontal: 16, 
    marginLeft: 10, 
    marginRight: 10, 
  },
  input: {
    height: 40, 
    width:300, 
    borderColor: "gray", 
    borderWidth: 1, 
    marginBottom: 15, 
    paddingHorizontal: 10, /* adiciona preenchimento */
  },
  buttonContainer: {
    display: 'flex', 
    flexDirection: "row", 
    justifyContent: 'center', 
    alignItems:"center", 
    gap: 35, /* espaçamento entre os itens  */
    marginTop: 20, 
  },
  text:{
    marginTop: 20, 
    fontSize: 16, 
    color: "black", /* cor do texto */
  },
  TextInput:{
    fontWeight: 'bold', 
    marginBottom: 15, 
  },
});

export default LoginScreen;
