import React from "react";
import { View, TextInput, Button, StyleSheet, Text } from "react-native";

const CadastroScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style = {styles.TextInput}>CADASTRE-SE AQUI!!!</Text>
        <TextInput style={styles.input} placeholder="Nome"/>
        <TextInput style={styles.input} placeholder="Email"/>
        <TextInput style={styles.input} placeholder="Telefone"/>
        <TextInput style={styles.input} placeholder="Senha" secureTextEntry={true}/>
      <View style={styles.buttonContainer}>
        <Button style={styles.button} title="Início" onPress={() => navigation.navigate('RGT')} color="orange" />
        <Button style={styles.button} title="Cadastrar" onPress={() => navigation.navigate('Home')} color="orange" />
        <Button style={styles.button} title="Login" onPress={() => navigation.navigate('Login')} color="orange" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: "center", 
    alignItems:"center", 
    marginLeft: 10, 
    marginRight: 10, 
    paddingHorizontal: 16, 
  },
  input: {
    height: 40,
    width:300, 
    borderColor: "gray", 
    borderWidth: 1, 
    marginBottom: 15, 
    paddingHorizontal: 10, 
  },
  buttonContainer: {
    display: 'flex', 
    flexDirection: "row", 
    justifyContent: 'center', 
    alignItems:"center",
    gap: 35, 
    marginTop: 20, 
  },
  text:{
    marginTop: 20, 
    fontSize: 16, 
    color: "black", 
  },

  TextInput:{
    fontWeight: 'bold',
    marginBottom: 15,
  },

});

export default CadastroScreen;
