import React, { useEffect } from "react";
import { StyleSheet, TextInput, View, Button, Text, Image} from "react-native";
// import { ScrollView } from "react-native-gesture-handler";
// import chile from "../assets/chile.jpeg";
// import bauneario from "../assets/bauneario.jpeg";
// import rio from "../assets/rio.jpeg";
// import coliseu from "../assets/coliseu.jpeg";
// import roma from "../assets/roma.jpeg";
// import betocarreiro from "../assets/betocarreiro.jpeg";
// import republicadominicana from "../assets/republicadominicana.jpeg";

function inicialScreen  ({ navigation })  {
  return (
    <View style={styles.container}>
      <Text style = {styles.TextInput}>BEM VINDO, CLIQUE NOS BOTÕES ABAIXO PARA PROSSEGUIR!!!</Text>
     <View style={styles.bt1}>
     <Button title="Ir para página de Login" onPress={ () =>  navigation.navigate("Login")} color="orange"></Button>
     </View>
     <View style={styles.bt2} >
      <Button title="Ir para página de Cadastro" onPress={ () => navigation.navigate("Cadastro")} color="orange"></Button>
     </View>
    </View> 
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: "center", /* itens no centro */
    justifyContent: "center", /* botões no centro*/
    flexDirection: "column", /* botões lado a lado*/
    backgroundColor: "writh", /*cor fundo*/  
  },
  image: {
    width: 200, 
    height: 200, 
    resizeMode: 'cover', 
    marginHorizontal: 10, 
  },
  bt1:{
    marginTop:50, /* margem superior */
  },
  bt2:{
    margin:50, /* margem */
  },
  text:{
    marginTop: 20, 
    fontSize: 16, /* tamanho da fonte */
    color: "black", /* cor do texto */
  },
  TextInput:{
    fontWeight: 'bold', /* deixar em negrito */
  }
});

export default inicialScreen;


// {/* <ScrollView>
//           <Image source={bauneario} style={styles.image} />
//           <Image source={chile} style={styles.image} />
//           <Image source={coliseu} style={styles.image} />
//           <Image source={rio} style={styles.image} />
//           <Image source={roma} style={styles.image} />
//           <Image source={betocarreiro} style={styles.image} />
//           <Image source={republicadominicana} style={styles.image} />
//       </ScrollView> */}